package mpd

import (
	"fmt"
	"log"
	"os"
	"testing"

	"barista.run/bar"
	server "barista.run/modules/mpd/internal"
	"barista.run/outputs"
	testBar "barista.run/testing/bar"
	gompd "github.com/fhs/gompd/v2/mpd"
	"golang.org/x/exp/maps"
)

var (
	serverRunning  = false
	useGoMPDServer = true
	// song filename present in dummy server's database
	songFirst = gompd.Attrs{
		"file": "song0042.ogg",
	}
	// song attributes placed at 2nd position in dummy server's playlist
	songNext = gompd.Attrs{
		"file":   "newcleus_jam-on-it.mp3",
		"artist": "Newcleus",
		"title":  "Jam On It",
		"genre":  "Electro",
	}
	// channel to communicate with internal testing server
	mpdChan chan bool
)

func TestMPD(t *testing.T) {
	var mpdMod, err = initMPD()
	if err != nil {
		t.Fatal("Failed to initialize MPD")
	}
	defer shutdownMPD(mpdMod)

	testBar.New(t)
	testBar.Run(mpdMod)

	// Initial event to trigger response on status channel
	if err := mpdMod.GetClient().Play(-1); err != nil {
		t.Error("Failed to start playing:", err)
	}
	testBar.NextOutput("first song").
		AssertText([]string{songFirst["file"] + " ▶"})
	if err := mpdMod.GetClient().Next(); err != nil {
		t.Error("Failed to switch to next song:", err)
	}
	testBar.NextOutput("second song").AssertText([]string{
		fmt.Sprintf("%s - %s ▶", songNext["title"], songNext["artist"]),
	})
	if err := mpdMod.GetClient().Pause(true); err != nil {
		t.Error("Failed to pause playback:", err)
	}
	// with modified output function
	iconState := maps.Clone(IconPlayback)
	iconState[Paused] = '|'
	mpdMod.Output(func(st Status) bar.Output {
		return outputs.Textf("%s : %s %c", st.Title, st.Artist, iconState[st.State])
	})
	testBar.NextOutput("second song paused").AssertText([]string{
		fmt.Sprintf("%s : %s |", songNext["title"], songNext["artist"]),
	})
}

func TestBadMPD(t *testing.T) {
	// Create & connect a client to a non-running server
	var mpdMod, err = newMPC()
	if err == nil {
		t.Error("MPD client not expected to be able to connect.")
		mpdMod.TearDown()
	}

	// Initialize server & client anew
	mpdMod, err = initMPD()
	if err != nil {
		t.Fatal("Failed to initialize MPD")
	}

	testBar.New(t)
	testBar.Run(mpdMod)

	if err := mpdMod.GetClient().Ping(); err != nil {
		t.Error("Can not ping a running server.")
	}

	if serverRunning && !stopServer() {
		t.Error("Failed to shutdown a running server!")
	}

	// Following operations on inaccessible server should fail.

	if err := mpdMod.GetClient().Ping(); err == nil {
		t.Error("Should not ping a terminated server.")
	}
}

// Returns new a instance of MPD client
func newMPC() (*Module[Status], error) {
	net, addr := localAddr()
	mod, err := New(ClientOpts{net, addr, "", []string{"player"}})
	if err != nil {
		return nil, err
	}
	return mod, nil
}

// Get testing MPD server network operating protocol and URI
func localAddr() (net, addr string) {
	if useGoMPDServer {
		// Don't clash with standard MPD port 6600
		return "tcp", "127.0.0.1:6603"
	}
	net = "unix"
	addr = os.Getenv("MPD_HOST")
	if len(addr) > 0 && addr[0] == '/' {
		return
	}
	net = "tcp"
	if len(addr) == 0 {
		addr = "127.0.0.1"
	}
	port := os.Getenv("MPD_PORT")
	if len(port) == 0 {
		port = "6600"
	}
	return net, addr + ":" + port
}

// Initialize MPD server listening and return assigned client
func initMPD() (mod *Module[Status], err error) {
	if startServer() {
		mod, err = newMPC()
		if err != nil {
			return nil, err
		}
		mod.GetClient().Clear()
		addPlaylist(mod.GetClient(), songFirst)
		addPlaylist(mod.GetClient(), songNext)
	} else {
		log.Fatalln("Failed to start the server!")
	}
	return mod, nil
}

// Unassign and free MPD client and stop server listening
func shutdownMPD(mod *Module[Status]) {
	mod.TearDown()
	if !stopServer() {
		log.Fatalln("Failed to stop the server!")
	}
}

// Starts a dummy MPD server instance.
// Returns true if it is running.
func startServer() bool {
	net, addr := localAddr()
	if !serverRunning && useGoMPDServer {
		mpdChan = make(chan bool)
		go server.Listen(net, addr, mpdChan)
		serverRunning = true
		<-mpdChan // wait until listening starts
	}
	return serverRunning
}

// Stops running dummy MPD server instance.
// Returns true when succeeded.
func stopServer() bool {
	if !serverRunning && useGoMPDServer {
		log.Println("Attempt to stop a not running server.")
		return false
	}
	// Notify MPD server to stop listening
	mpdChan <- false
	// Close channel after notification has been received
	close(mpdChan)
	serverRunning = false
	return true
}

// Factored-out verbose error handling of that simple operation
// of adding a song to current playlist.
func addPlaylist(mpc *gompd.Client, song gompd.Attrs) error {
	if err := mpc.Add(song["file"]); err != nil {
		return err
	}
	return nil
}

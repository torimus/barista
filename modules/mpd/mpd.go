package mpd

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"barista.run/bar"
	"barista.run/base/value"
	l "barista.run/logging"
	"barista.run/outputs"
	gompd "github.com/fhs/gompd/v2/mpd"

	"golang.org/x/exp/maps"
)

// Module represents a bar.Module that displays MPD status information.
type Module[T statKind] struct {
	outputFunc value.Value
	client     atomic.Pointer[gompd.Client]
	watcher    atomic.Pointer[gompd.Watcher]
}

// MPD client connection options
type ClientOpts struct {
	Protocol string   // tcp or unix
	Address  string   // hostname:port or /absolute/path/to/mpd.socket
	Password string   // plaintext password (optional)
	Subsys   []string // list of monitored subsystems (empty for all)
}

// Opinionated subset of information attributes, correspondingly typed
// For a complete list see documentation at https://mpd.readthedocs.io/en/latest/protocol.html#querying-mpd-s-status
type Status struct {
	Album          string
	Artist         string
	Composer       string
	Date           string // song's release date
	Duration       time.Duration
	Elapsed        time.Duration
	File           string
	Genre          string
	Performer      string
	PlaylistLength uint
	Position       uint // song position in playlist
	State          playbackState
	Title          string
	Track          uint // track number within album
}

// Dynamic status for customized access to any available attribute
type DynStatus gompd.Attrs

type (
	playbackState          string
	statKind               interface{ Status | DynStatus }
	OutputFunc[T statKind] func(T) bar.Output
)

type (
	MPWError error
	MPCError error
)

const (
	Paused  playbackState = "pause"
	Playing playbackState = "play"
	Stopped playbackState = "stop"
	Unknown playbackState = "unknown"
)

var (
	DefaultOpts  = ClientOpts{"tcp", ":6060", "", []string{"player"}}
	IconPlayback = map[playbackState]rune{
		Paused:  '⏸',
		Playing: '▶',
		Stopped: '■',
		Unknown: '❓',
	}
)

func New(opts ClientOpts) (*Module[Status], error) {
	return NewGen[Status](opts, defaultOutFunc)
}

func NewGen[T statKind](opts ClientOpts, outFn OutputFunc[T]) (*Module[T], error) {
	mod := new(Module[T])

	l.Register(mod, "outputFunc")

	// Instantiate MPD server events watcher
	mpw, err := gompd.NewWatcher(opts.Protocol, opts.Address, opts.Password, opts.Subsys...)
	if err != nil {
		l.Log("MPD watcher connection has failed: %v", err)
		return mod, MPWError(errors.New(err.Error()))
	}
	mod.watcher.Store(mpw)

	// MPD client connection instance
	mpc, err := gompd.DialAuthenticated(opts.Protocol, opts.Address, opts.Password)
	if err != nil {
		l.Log("Connection to MPD has failed: %v", err)
		return mod, MPCError(errors.New(err.Error()))
	}
	mod.client.Store(mpc)

	// Output function fills-in information about current track
	mod.Output(outFn)

	return mod, nil
}

// Output configures a module to display the output of a user-defined
// function.
func (mod *Module[T]) Output(outFunc OutputFunc[T]) *Module[T] {
	mod.outputFunc.Set(outFunc)
	return mod
}

// Default output function returns title & artist if available,
// or source filename as a fallback, plus a playing status symbol.
func defaultOutFunc(stat Status) bar.Output {
	var out string
	if strings.TrimSpace(stat.Title) == "" {
		out = fmt.Sprintf("%s", stat.File)
	} else {
		out = fmt.Sprintf("%s - %s", stat.Title, stat.Artist)
	}
	return outputs.Textf("%s %c", out, IconPlayback[stat.State])
}

// Starts push-based streaming of information updates invoked by mpd watcher events
func (mod *Module[T]) Stream(snk bar.Sink) {
	var mpdStat value.ErrorValue
	var stErr error

	// status information channel
	// stat, stErr := mpdStat.Get()
	nextStat, done := mpdStat.Subscribe()
	defer done()

	// output formatting function channel
	outputFunc := mod.outputFunc.Get().(OutputFunc[T])
	nextOutFunc, done := mod.outputFunc.Subscribe()
	defer done()

	go func() {
		for {
			select {
			case <-mod.watcher.Load().Event:
				mod.updateStatus(&mpdStat)
			case err := <-mod.watcher.Load().Error:
				log.Println("→ mpd error", err)
			}
		}
		// listen to subset of selected mpd events
		// for _ev := range mod.watcher.Load().Event {
		// 	log.Println("→ mpd event", _ev)
		// 	mod.updateStatus(&mpdStat)
		// }
	}()

EventsReactor:
	for {
		// check if output receiver is ready
		if snk.Error(stErr) {
			return
		}

		select {
		case <-nextStat:
			stat, stErr := mpdStat.Get()
			if stErr != nil {
				log.Println("Failure at MPD status retrieval: ", stErr)
				break EventsReactor
			}
			if attr, ok := stat.(T); ok {
				snk.Output(outputs.Group(outputFunc(attr)).
					OnClick(defaultClickHandler(mod)))
			}
		case <-nextOutFunc:
			outputFunc = mod.outputFunc.Get().(OutputFunc[T])
			// default:
			// 	log.Println("→ no client response")
		}
	}
}

// Attempt to close MPD client and free acquired resources
func (mod *Module[T]) TearDown() error {
	mpc := mod.GetClient()
	if mpc != nil {
		if err := mpc.Close(); err != nil {
			return fmt.Errorf("MPD client failed to close: %s", err)
		}
	}
	return nil
}

// Fills-in MPD status information with actual values
func (mod *Module[T]) updateStatus(stat *value.ErrorValue) {
	attr, err := mod.GetClient().Status()
	if err != nil {
		l.Log("Failed to retrieve MPD status: %v", err)
		return
	}
	attr = normalizeMap((*map[string]string)(&attr))
	song, err := mod.GetClient().CurrentSong()
	if err != nil {
		l.Log("Failed to retrieve current song: %v", err)
		return
	}
	song = normalizeMap((*map[string]string)(&song))

	var kind T
	switch any(kind).(type) {
	case Status:
		duration, _ := time.ParseDuration(attr["duration"])
		elapsed, _ := time.ParseDuration(attr["elapsed"])
		plistLen, _ := strconv.ParseUint(attr["playlistlength"], 10, 32)
		songPos, _ := strconv.ParseUint(attr["song"], 10, 32)
		track, _ := strconv.ParseUint(attr["track"], 10, 32)
		stat.Set(Status{
			Album:          song["album"],
			Artist:         song["artist"],
			Composer:       song["composer"],
			Date:           song["date"],
			Duration:       duration,
			Elapsed:        elapsed,
			File:           song["file"],
			Genre:          song["genre"],
			Performer:      song["performer"],
			PlaylistLength: uint(plistLen),
			Position:       uint(songPos),
			State:          playbackState(attr["state"]),
			Title:          song["title"],
			Track:          uint(track),
		})
	case DynStatus:
		maps.Copy(attr, song)
		stat.Set(DynStatus(attr))
	default:
		l.Log("Unsupported kind of status: %T", kind)
	}
}

// Accessor method for a convenience
func (mod *Module[T]) GetClient() *gompd.Client {
	return mod.client.Load()
}

func defaultClickHandler[T statKind](mod *Module[T]) func(bar.Event) {
	return func(e bar.Event) {
		switch e.Button {
		case bar.ButtonLeft:
			paused := GetPlaybackState(mod) == Paused
			mod.GetClient().Pause(!paused)
		case bar.ButtonRight:
			mod.GetClient().Next()
		case bar.ButtonMiddle:
			mod.GetClient().Previous()
		}
	}
}

// Fetches current playback status
func GetPlaybackState[T statKind](mod *Module[T]) playbackState {
	att, err := mod.GetClient().Status()
	if err != nil {
		return playbackState(att["state"])
	}
	return Unknown
}

func normalizeMap[T any](m *map[string]T) map[string]T {
	res := make(map[string]T)
	for k, v := range *m {
		res[strings.ToLower(k)] = v
	}
	return res
}
